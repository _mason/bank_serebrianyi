package Main;

import enums.Operation;

public class Transaction {

    private Account source;     // - счет
    private Operation operation;// - операция
    private double amount;      // - сумма

    public Transaction(Account source, Operation operation, double amount) {
        this.source = source;
        this.operation = operation;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "source=" + source +
                ", operation=" + operation +
                ", amount=" + amount +
                '}';
    }
}
