package Main;

import enums.Operation;

import java.util.Arrays;

public class Bank {

    private Client[] clients;
    private Account[] accounts;
    private Transaction[] transactions;

    public void addAccount(Client client) {
        Account account = new Account(client, (double) 0);
        accounts = Arrays.copyOf(accounts, accounts.length + 1);
        accounts[accounts.length - 1] = account;
    }

    public boolean addClient(Client client) {
        if (findClient(client) == null) {
            clients = Arrays.copyOf(clients, clients.length + 1);
            clients[clients.length - 1] = client;
            return true;
        } else {
            System.out.println("Client is in base");
            return false;
        }
    }

    public Client findClient(Client searchClient) {
        for (int i = 0; i < clients.length; i++) {
            if (clients[i].equals(searchClient)) return searchClient;
        }
        return null;
    }

    public boolean putIntoAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if ((account != null) && (amount > 0)) {
            Transaction transaction = new Transaction(account, Operation.PUT, amount);
            transactions = Arrays.copyOf(transactions, transactions.length + 1);
            transactions[transactions.length - 1] = transaction;
            account.increaseBalance(amount);
            return true;
        } else
            System.out.println("it is impossible to perform the operation");
        return false;
    }

    public Account findAccount(long searchAccountNumber) {
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].getAccountNumber().equals(searchAccountNumber)) return accounts[i];
        }
        return null;
    }

    public boolean withdrawFromAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if ((account != null) && (amount > 0) && (account.getBalance() > amount)) {
            Transaction transaction = new Transaction(account, Operation.WITHDRAW, amount);
            transactions = Arrays.copyOf(transactions, transactions.length + 1);
            transactions[transactions.length - 1] = transaction;
            account.decreaseBalance(amount);
            return true;
        } else {
            System.out.println("it is impossible to perform the operation");
            return false;
        }
    }

    public boolean moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount) {
        if (findAccount(sourceAccountNumber) != null && findAccount(destinationAccountNumber) != null) {
            withdrawFromAccount(sourceAccountNumber, amount);
            putIntoAccount(destinationAccountNumber, amount);
            return true;
        } else {
            System.out.println("it is impossible to perform the operation");
            return false;
        }
    }

    public Account[] findAccountsByClient(Client searchClient) {
        Account[] acc = new Account[0];
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].getClient().equals(searchClient)) {
                acc = Arrays.copyOf(acc, acc.length + 1);
                acc[acc.length - 1] = accounts[i];
            }
        }
        return acc;
    }

    public void printClients() {
        for (int i = 0; i < clients.length; i++) {
            System.out.println(clients[i].toString());
        }
    }

    public void printAccounts() {
        for (int i = 0; i < accounts.length; i++) {
            System.out.println(accounts[i].toString());
        }
    }

    public void printAccountsForClient(Client client) {
        for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].getClient().equals(client)) System.out.println(accounts[i].toString());
        }
    }

    public void printTransactions() {
        for (int i = 0; i < transactions.length; i++) {
            System.out.println(transactions[i].toString());
        }
    }
}
